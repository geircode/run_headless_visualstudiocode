import pprint
import os
import configparser
from code_server.JsonHelper import JsonHelper, JsonBase
from pathlib import Path
import docker

pp = pprint.PrettyPrinter(indent=4)

class DockerService:

    def getContainers(self):
        client = docker.from_env()
        return client.containers.list()
        
if __name__ == '__main__':
    obj = DockerService().getContainers()
    pass