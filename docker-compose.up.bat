cd %~dp0
docker rm -f headless_vscode-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
REM wait for 1-2 seconds for the container to start
pause
REM docker exec -it headless_vscode-1 /bin/bash
docker logs -f headless_vscode-1