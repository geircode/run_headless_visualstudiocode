FROM codercom/code-server:1.621 AS base

USER root

RUN apt-get update && apt-get install -y \
	python3 python3-pip

FROM base

WORKDIR /app
COPY . /app

RUN pip3 install -r requirements.txt 
# ENTRYPOINT tail -f /dev/null


